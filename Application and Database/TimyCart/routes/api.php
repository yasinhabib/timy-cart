<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('product','ProductController@get');
Route::get('/product/{Product_id}','ProductController@getDetail');
Route::post('/product','ProductController@post');
Route::put('/product/{Product_id}','ProductController@update');
Route::delete('/product/{Product_id}','ProductController@delete');

Route::get('/brand','ProductBrandController@get');
Route::get('/brand/{Brand_id}','ProductBrandController@getDetail');
Route::post('/brand','ProductBrandController@post');
Route::put('/brand/{Brand_id}','ProductBrandController@update');
Route::delete('/brand/{Brand_id}','ProductBrandController@delete');

Route::get('/category','ProductCategoryController@get');
Route::get('/category/{Category_id}','ProductCategoryController@getDetail');
Route::post('/category','ProductCategoryController@post');
Route::put('/category/{Category_id}','ProductCategoryController@update');
Route::delete('/category/{Category_id}','ProductCategoryController@delete');

Route::get('/product-detail','ProductDetailController@get');
Route::get('/product-detail/{Product_detail_id}','ProductDetailController@getDetail');
Route::post('/product-detail','ProductDetailController@post');
Route::put('/product-detail/{Product_detail_id}','ProductDetailController@update');
Route::delete('/product-detail/{Product_detail_id}','ProductDetailController@delete');

Route::get('/product-image','ProductImageController@get');
Route::get('/product-image/{Product_image_id}','ProductImageController@getDetail');
Route::post('/product-image','ProductImageController@post');
Route::put('/product-image/{Product_image_id}','ProductImageController@update');
Route::delete('/product-image/{Product_image_id}','ProductImageController@delete');