<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::view('/', 'welcome');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => array('auth')], function () {

	Route::get('/product','ProductController@index');
	Route::get('/product/get','ProductController@get');
	Route::get('/product/get/{Product_id}','ProductController@getDetail');
	Route::post('/product/insert','ProductController@post');
	Route::post('/product/update/{Product_id}','ProductController@put');
	Route::post('/product/delete/{Product_id}','ProductController@delete');

	Route::get('/brand','ProductBrandController@index');
	Route::get('/brand/get','ProductBrandController@get');
	Route::get('/brand/get/{Brand_id}','ProductBrandController@getDetail');
	Route::post('/brand/insert','ProductBrandController@post');
	Route::post('/brand/update/{Brand_id}','ProductBrandController@put');
	Route::post('/brand/delete/{Brand_id}','ProductBrandController@delete');

	Route::get('/category','ProductCategoryController@index');
	Route::get('/category/get','ProductCategoryController@get');
	Route::get('/category/get/{Category_id}','ProductCategoryController@getDetail');
	Route::post('/category/insert','ProductCategoryController@post');
	Route::post('/category/update/{Category_id}','ProductCategoryController@put');
	Route::post('/category/delete/{Category_id}','ProductCategoryController@delete');

	Route::get('/product-detail','ProductDetailController@index');
	Route::get('/product-detail/get','ProductDetailController@get');
	Route::get('/product-detail/get/{Product_detail_id}','ProductDetailController@getDetail');
	Route::post('/product-detail/insert','ProductDetailController@post');
	Route::post('/product-detail/update/{Product_detail_id}','ProductDetailController@put');
	Route::post('/product-detail/delete/{Product_detail_id}','ProductDetailController@delete');

	Route::get('/product-image','ProductImageController@index');
	Route::get('/product-image/get','ProductImageController@get');
	Route::get('/product-image/get/{Product_image_id}','ProductImageController@getDetail');
	Route::post('/product-image/insert','ProductImageController@post');
	Route::post('/product-image/update/{Product_image_id}','ProductImageController@put');
	Route::post('/product-image/delete/{Product_image_id}','ProductImageController@delete');
});	
