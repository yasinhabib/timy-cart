@extends('layouts.app')

@section('content')
<div class="row">
 	<div class="col-md-12 mt-2">
 		<h1 class="float-left m-4">Category List</h1>
 		<button type="button" class="btn btn-md btn-info float-right m-4" data-toggle="modal" data-target="#ModalCategory" id="btnModalCategory">Add New Category</button>
	</div>
</div>	
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-hover table-sm" id="category-table">
 			<thead>
	 			<tr>
	 				<th>
	 					#
	 				</th>
	 				<th>
	 					Category ID
	 				</th>
	 				<th>
	 					Category Name
	 				</th>
	 				<th>
	 					Action
	 				</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 		
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('js')
	<script>
		var categorytable = $('#category-table').DataTable({
            processing: true,
		        serverSide: true,
		        ajax: '{{ url('/category/get') }}',
		        timeout: 60000,
		        columns: [
		        	{data: 'Category_id',width: "16px"},
		            {data: 'Category_id',width: "16px"},
		            {data: 'Category_name',width: "100px"},
		            {data: 'Category_id',width: "40px"},
		        ],
		        bPaginate: true,
                searching : true,
                bSort: true,
                bInfo: true,
	            scrollX: true,
	            scrollY: '100vh',
	            scrollCollapse: true,
		        autoWidth: false,
                order: [[ 1, 'asc' ]],
		        columnDefs: [
		        		{
		        			"targets": [0],
							"createdCell": function (td, cellData, rowData, row, col) {
								$(td).text(row+1);
							}, 
                            orderable: false
		        		},
						{
                            "targets": [3],
                            "data": null,
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).empty();
                                $(td).append($('@include('inc.buttons.modifyRecord')').addClass('mx-1'))
                                    .append($('@include('inc.buttons.deleteRecord')').addClass('mx-1 mt-1'))
                            },
                        }
				],
        });

        $('#btnModalCategory').on('click',function(){
        	$('#Category_id').val('<AUTO>');
        	$('#ModalCategory .btn-update').addClass('d-none');
        	$('#ModalCategory .btn-save').removeClass('d-none');
        })

        $('#ModalCategory').on('click','.btn-save',function(){
        	var Category_name = $('#Category_name').val();

        	$.ajax({
                url: '{{ url('/category/insert') }}',
                data: {Category_name: Category_name},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                            	$('#ModalCategory').modal('hide');
                                categorytable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#category-table').on('click','.btn-modify-record',function(){
        	var tr = $(this).closest('tr');

            var index = categorytable.row( tr ).index();

            data = categorytable.row(index).data();

            $('#ModalCategory .btn-save').addClass('d-none');
        	$('#ModalCategory .btn-update').removeClass('d-none');

            $('#Category_id').val(data.Category_id);
            $('#Category_name').val(data.Category_name);

            $('#ModalCategory').modal();
        })

        $('#ModalCategory').on('click','.btn-update',function(){
        	var Category_id = $('#Category_id').val();
        	var Category_name = $('#Category_name').val();

        	$.ajax({
                url: '{{ url('/category/update') }}/'+Category_id,
                data: {Category_name: Category_name},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                            	$('#ModalCategory').modal('hide');
                                categorytable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#category-table').on('click','.btn-remove-record',function(){
        	var tr = $(this).closest('tr');

            var index = categorytable.row( tr ).index();

            data = categorytable.row(index).data();

            $.confirm({
                title: 'Confirmation',
                content: 'Are you sure to delete this record?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
						$.ajax({
			                url: '{{ url('/category/delete') }}/'+data.Category_id,
			                type: 'POST',
			                dataType: "JSON",
			                success: function(data)
			                {
			                    var message = '';
			                    message = data.Message.replace(/\n/g, "<br />");
			                    $.alert({
			                        title: 'Information',
			                        content: message,
			                        buttons: {
			                            ok: function () {
			                            	$('#ModalCategory').modal('hide');
			                                categorytable.ajax.reload(null,false);
			                            },
			                        }
			                    });                        
			                },
			                error: function (jqXHR, textStatus, errorThrown){
			                    var errorMsg = 'Ajax request failed: ' + errorThrown;
			                    console.log(errorMsg);
			                }
			            });
					}
			    }
			})
        })
	</script>
@endsection

@include('modals.category')