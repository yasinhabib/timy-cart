@extends('layouts.app')

@section('content')
<div class="row">
 	<div class="col-md-12 mt-2">
 		<h1 class="float-left m-4">Brands List</h1>
 		<button type="button" class="btn btn-md btn-info float-right m-4" data-toggle="modal" data-target="#ModalBrand" id="btnModalBrand">Add New Brand</button>
	</div>
</div>	
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-hover table-sm" id="brand-table">
 			<thead>
	 			<tr>
	 				<th>
	 					#
	 				</th>
	 				<th>
	 					Brand ID
	 				</th>
	 				<th>
	 					Brand Name
	 				</th>
	 				<th>
	 					Action
	 				</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 		
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('js')
	<script>
		var brandtable = $('#brand-table').DataTable({
            processing: true,
		        serverSide: true,
		        ajax: '{{ url('/brand/get') }}',
		        timeout: 60000,
		        columns: [
		        	{data: 'Brand_id',width: "16px"},
		            {data: 'Brand_id',width: "16px"},
		            {data: 'Brand_name',width: "100px"},
		            {data: 'Brand_id',width: "40px"},
		        ],
		        bPaginate: true,
                searching : true,
                bSort: true,
                bInfo: true,
	            scrollX: true,
	            scrollY: '100vh',
	            scrollCollapse: true,
		        autoWidth: false,
                order: [[ 1, 'asc' ]],
		        columnDefs: [
		        		{
		        			"targets": [0],
							"createdCell": function (td, cellData, rowData, row, col) {
								$(td).text(row+1);
							}, 
                            orderable: false
		        		},
						{
                            "targets": [3],
                            "data": null,
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).empty();
                                $(td).append($('@include('inc.buttons.modifyRecord')').addClass('mx-1'))
                                    .append($('@include('inc.buttons.deleteRecord')').addClass('mx-1 mt-1'))
                            },
                        }
				],
        });

        $('#btnModalBrand').on('click',function(){
        	$('#Brand_id').val('<AUTO>');
        	$('#ModalBrand .btn-update').addClass('d-none');
        	$('#ModalBrand .btn-save').removeClass('d-none');
        })

        $('#ModalBrand').on('click','.btn-save',function(){
        	var Brand_name = $('#Brand_name').val();

        	$.ajax({
                url: '{{ url('/brand/insert') }}',
                data: {Brand_name: Brand_name},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                            	$('#ModalBrand').modal('hide');
                                brandtable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#brand-table').on('click','.btn-modify-record',function(){
        	var tr = $(this).closest('tr');

            var index = brandtable.row( tr ).index();

            data = brandtable.row(index).data();

            $('#ModalBrand .btn-save').addClass('d-none');
        	$('#ModalBrand .btn-update').removeClass('d-none');

            $('#Brand_id').val(data.Brand_id);
            $('#Brand_name').val(data.Brand_name);

            $('#ModalBrand').modal();
        })

        $('#ModalBrand').on('click','.btn-update',function(){
        	var Brand_id = $('#Brand_id').val();
        	var Brand_name = $('#Brand_name').val();

        	$.ajax({
                url: '{{ url('/brand/update') }}/'+Brand_id,
                data: {Brand_name: Brand_name},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                            	$('#ModalBrand').modal('hide');
                                brandtable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#brand-table').on('click','.btn-remove-record',function(){
        	var tr = $(this).closest('tr');

            var index = brandtable.row( tr ).index();

            data = brandtable.row(index).data();

            $.confirm({
                title: 'Confirmation',
                content: 'Are you sure to delete this record?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
						$.ajax({
			                url: '{{ url('/brand/delete') }}/'+data.Brand_id,
			                type: 'POST',
			                dataType: "JSON",
			                success: function(data)
			                {
			                    var message = '';
			                    message = data.Message.replace(/\n/g, "<br />");
			                    $.alert({
			                        title: 'Information',
			                        content: message,
			                        buttons: {
			                            ok: function () {
			                            	$('#ModalBrand').modal('hide');
			                                brandtable.ajax.reload(null,false);
			                            },
			                        }
			                    });                        
			                },
			                error: function (jqXHR, textStatus, errorThrown){
			                    var errorMsg = 'Ajax request failed: ' + errorThrown;
			                    console.log(errorMsg);
			                }
			            });
					}
			    }
			})
        })
	</script>
@endsection

@include('modals.brand')