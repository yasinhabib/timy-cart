@extends('layouts.app')

@section('content')
<div class="row">
 	<div class="col-md-12 mt-2">
 		<h1 class="float-left m-4">Product List</h1>
 		<button type="button" class="btn btn-md btn-info float-right m-4" data-toggle="modal" data-target="#ModalProduct" id="btnModalProduct">Add New Product</button>
	</div>
</div>	
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-hover table-sm" id="product-table" width="100%">
 			<thead>
	 			<tr>
	 				<th>
	 					#
	 				</th>
                    <th>
                        Product ID
                    </th>
	 				<th>
	 					Brand
	 				</th>
	 				<th>
	 					Category
	 				</th>
                    <th>
                        Product Name
                    </th>
                    <th>
                        Status
                    </th>
	 				<th>
	 					Action
	 				</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 		
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('js')
	<script>
        $( document ).ready(function(){
            $.ajax({
                url : '{{ url('/brand/get') }}',
                type: "GET",
                dataType: "JSON",
                success: function(result)
                {   
                    result.data.forEach(function(element,index) {
                        $('#Brand_id').append(
                            $('<option>')
                                .val(element.Brand_id)
                                .text(element.Brand_name)
                        )
                    });
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                    hideLoading();
                    setTimeout(function () {
                        getSystemDate();
                    }, 500)
                }
            });

            $.ajax({
                url : '{{ url('/category/get') }}',
                type: "GET",
                dataType: "JSON",
                success: function(result)
                {   
                    result.data.forEach(function(element,index) {
                        $('#Category_id').append(
                            $('<option>')
                                .val(element.Category_id)
                                .text(element.Category_name)
                        )
                    });
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                    hideLoading();
                    setTimeout(function () {
                        getSystemDate();
                    }, 500)
                }
            });
        })

		var producttable = $('#product-table').DataTable({
            processing: true,
		        serverSide: true,
		        ajax: '{{ url('/product/get') }}',
		        timeout: 60000,
		        columns: [
		        	{data: 'Product_id',width: "16px"},
		            {data: 'Product_id',width: "100px"},
		            {data: 'product_brand.Brand_name',width: "100px"},
                    {data: 'product_category.Category_name',width: "100px"},
                    {data: 'Product_Name',width: "100px"},
                    {data: 'Product_status',width: "100px"},
		            {data: 'Product_id',width: "50px"},
		        ],
		        bPaginate: true,
                searching : true,
                bSort: true,
                bInfo: true,
	            scrollX: true,
	            scrollY: '100vh',
	            scrollCollapse: true,
		        autoWidth: false,
                order: [[ 1, 'asc' ]],
		        columnDefs: [
		        		{
		        			"targets": [0],
							"createdCell": function (td, cellData, rowData, row, col) {
								$(td).text(row+1);
							}, 
                            orderable: false
		        		},
                        {
                            "targets": [5],
                            "createdCell": function (td, cellData, rowData, row, col) {
                                if(cellData == 1){
                                    $(td).text('Enabled');
                                }else{
                                    $(td).text('Disabled');
                                }

                            }
                        },
						{
                            "targets": [6],
                            "data": null,
                            "createdCell": function (td, cellData, rowData, row, col) {
                                $(td).empty();
                                $(td).append($('@include('inc.buttons.modifyRecord')').addClass('mx-1'))
                                    .append($('@include('inc.buttons.deleteRecord')').addClass('mx-1 mt-1'))
                                    .append($('@include('inc.buttons.detailRecord')').addClass('mx-1 mt-1'))
                                    .append($('@include('inc.buttons.imagesRecord')').addClass('mx-1 mt-1'))
                            },
                        }
				],
        });

        $('#btnModalProduct').on('click',function(){
        	$('#Product_id').val('<AUTO>');
        	$('#ModalProduct .btn-update').addClass('d-none');
        	$('#ModalProduct .btn-save').removeClass('d-none');
        })

        $('#ModalProduct').on('click','.btn-save',function(){
        	var Brand_id = $('#Brand_id').val();
            var Category_id = $('#Category_id').val();
            var Product_Name = $('#Product_name').val();
            var Product_status = $('#Product_status').val();

        	$.ajax({
                url: '{{ url('/product/insert') }}',
                data: {Brand_id: Brand_id,Category_id: Category_id,Product_Name: Product_Name,Product_status: Product_status},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                            	$('#ModalProduct').modal('hide');
                                producttable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#product-table').on('click','.btn-modify-record',function(){
        	var tr = $(this).closest('tr');

            var index = producttable.row( tr ).index();

            data = producttable.row(index).data();

            $('#ModalProduct .btn-save').addClass('d-none');
        	$('#ModalProduct .btn-update').removeClass('d-none');

            $('#Product_id').val(data.Product_id);
            $('#Brand_id').val(data.product_brand.Brand_id);
            $('#Category_id').val(data.product_category.Category_id);
            $('#Product_name').val(data.Product_Name);
            $('#Product_status').val(data.Product_status);

            $('#ModalProduct').modal();
        })

        $('#ModalProduct').on('click','.btn-update',function(){
        	var Product_id = $('#Product_id').val();
        	var Brand_id = $('#Brand_id').val();
            var Category_id = $('#Category_id').val();
            var Product_Name = $('#Product_name').val();
            var Product_status = $('#Product_status').val();

        	$.ajax({
                url: '{{ url('/product/update') }}/'+Product_id,
                data: {Brand_id: Brand_id,Category_id: Category_id,Product_Name: Product_Name,Product_status: Product_status},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                            	$('#ModalProduct').modal('hide');
                                producttable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#product-table').on('click','.btn-remove-record',function(){
        	var tr = $(this).closest('tr');

            var index = producttable.row( tr ).index();

            data = producttable.row(index).data();

            $.confirm({
                title: 'Confirmation',
                content: 'Are you sure to delete this record?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
						$.ajax({
			                url: '{{ url('/product/delete') }}/'+data.Product_id,
			                type: 'POST',
			                dataType: "JSON",
			                success: function(data)
			                {
			                    var message = '';
			                    message = data.Message.replace(/\n/g, "<br />");
			                    $.alert({
			                        title: 'Information',
			                        content: message,
			                        buttons: {
			                            ok: function () {
			                            	$('#ModalProduct').modal('hide');
			                                producttable.ajax.reload(null,false);
			                            },
			                        }
			                    });                        
			                },
			                error: function (jqXHR, textStatus, errorThrown){
			                    var errorMsg = 'Ajax request failed: ' + errorThrown;
			                    console.log(errorMsg);
			                }
			            });
					}
			    }
			})
        })

        $('#product-table').on('click','.btn-detail-record',function(){
            var tr = $(this).closest('tr');

            var index = producttable.row( tr ).index();

            data = producttable.row(index).data();

            $('#ModalProduct .btn-save').addClass('d-none');
            $('#ModalProduct .btn-update').removeClass('d-none');

            
            $('#Product_detail_id').val('<AUTO>');
            $('#Sku_number').val('');
            $('#Description').val('');
            $('#Specification').val('');
            $('#Link_rewrite').val('');
            
            $('#Detail_Product_id').val(data.Product_id);

            if(data.product_detail.length != 0){
                $('#Product_detail_id').val(data.product_detail[0].Product_detail_id);
                $('#Sku_number').val(data.product_detail[0].Sku_number);
                $('#Description').val(data.product_detail[0].Description);
                $('#Specification').val(data.product_detail[0].Specification);
                $('#Link_rewrite').val(data.product_detail[0].Link_rewrite);

                $('#ModalProductDetail .btn-save').addClass('d-none');
                $('#ModalProductDetail .btn-update').removeClass('d-none');
            }else{
                $('#ModalProductDetail .btn-save').removeClass('d-none');
                $('#ModalProductDetail .btn-update').addClass('d-none');
            }

            $('#ModalProductDetail').modal();
        })

        $('#ModalProductDetail').on('click','.btn-save',function(){
            var Product_id = $('#Detail_Product_id').val();
            var Description = $('#Description').val();
            var Specification = $('#Specification').val();
            var Sku_number = $('#Sku_number').val();
            var Link_rewrite = $('#Link_rewrite').val();

            $.ajax({
                url: '{{ url('/product-detail/insert') }}',
                data: {Product_id: Product_id,Description: Description,Specification: Specification,Sku_number: Sku_number,Link_rewrite: Link_rewrite},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                                $('#ModalProductDetail').modal('hide');
                                producttable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#ModalProductDetail').on('click','.btn-update',function(){
            var Product_detail_id = $('#Product_detail_id').val();
            var Product_id = $('#Detail_Product_id').val();
            var Description = $('#Description').val();
            var Specification = $('#Specification').val();
            var Link_rewrite = $('#Link_rewrite').val();

            $.ajax({
                url: '{{ url('/product-detail/update') }}/'+Product_detail_id,
                data: {Product_id: Product_id,Description: Description,Specification: Specification,Sku_number: Sku_number,Link_rewrite: Link_rewrite},
                type: 'POST',
                dataType: "JSON",
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                                $('#ModalProductDetail').modal('hide');
                                producttable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#product-table').on('click','.btn-images-record',function(){
            var tr = $(this).closest('tr');

            var index = producttable.row( tr ).index();

            data = producttable.row(index).data();

            $('#ModalProduct .btn-save').addClass('d-none');
            $('#ModalProduct .btn-update').removeClass('d-none');

            
            $('#Product_image_id').val('<AUTO>');
            $('#Product_image_name').val('');
            
            $('#Image_Product_id').val(data.Product_id);

            if(data.product_image.length != 0){
                $('#Product_image_id').val(data.product_image[0].Product_image_id);
                // $('#Product_image_name').val(data.product_image[0].Product_image_name);

                $('#ModalProductImage .btn-save').addClass('d-none');
                $('#ModalProductImage .btn-update').removeClass('d-none');
            }else{
                $('#ModalProductImage .btn-save').removeClass('d-none');
                $('#ModalProductImage .btn-update').addClass('d-none');
            }

            $('#ModalProductImage').modal();
        })

        $('#ModalProductImage').on('click','.btn-save',function(){
            var Product_id = $('#Image_Product_id').val();
            var Product_image_name = $('#Product_image_name')[0].files[0];

            var formData = new FormData();
            formData.append("Product_image_name", Product_image_name); 
            formData.append("Product_id", Product_id); 

            $.ajax({
                url: '{{ url('/product-image/insert') }}',
                data: formData,
                type: 'POST',
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                                $('#ModalProductImage').modal('hide');
                                producttable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })

        $('#ModalProductImage').on('click','.btn-update',function(){
            var Product_image_id = $('#Product_image_id').val();
            var Product_id = $('#Image_Product_id').val();
            var Product_image_name = $('#Product_image_name')[0].files[0];

            var formData = new FormData();
            formData.append("Product_image_name", Product_image_name); 
            formData.append("Product_id", Product_id); 

            $.ajax({
                url: '{{ url('/product-image/update') }}/'+Product_image_id,
                data: formData,
                type: 'POST',
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function(data)
                {
                    var message = '';
                    message = data.Message.replace(/\n/g, "<br />");
                    $.alert({
                        title: 'Information',
                        content: message,
                        buttons: {
                            ok: function () {
                                $('#ModalProductImage').modal('hide');
                                producttable.ajax.reload(null,false);
                            },
                        }
                    });                        
                },
                error: function (jqXHR, textStatus, errorThrown){
                    var errorMsg = 'Ajax request failed: ' + errorThrown;
                    console.log(errorMsg);
                }
            });
        })
	</script>
@endsection

@include('modals.product')
@include('modals.productDetail')
@include('modals.productImage')