<div class="modal" id="ModalProduct" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Insert Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="Product_id" class="col-sm-4 col-form-label">Product ID</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Product_id" readonly="readonly">
          </div>
        </div>
        <div class="form-group row">
          <label for="Brand_id" class="col-sm-4 col-form-label">Brand</label>
          <div class="col-sm-8">
            <select class="form-control" id="Brand_id">
              
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="Category_id" class="col-sm-4 col-form-label">Category</label>
          <div class="col-sm-8">
            <select class="form-control" id="Category_id">
              
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="Product_name" class="col-sm-4 col-form-label">Product Name</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Product_name">
          </div>
        </div>
        <div class="form-group row">
          <label for="Product_status" class="col-sm-4 col-form-label">Status</label>
          <div class="col-sm-8">
            <select class="form-control" id="Product_status">
              <option value="1">Enabled</option>
              <option value="0">Disabled</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-close" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
        <button type="button" class="btn btn-primary btn-sm btn-save"><i class="fas fa-save"></i> Save</button>
        <button type="button" class="btn btn-primary btn-sm btn-update"><i class="fas fa-save"></i> Update</button>
      </div>
    </div>
  </div>
</div>