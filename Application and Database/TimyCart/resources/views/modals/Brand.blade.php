<div class="modal" id="ModalBrand" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Insert Brand</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="Brand_id" class="col-sm-4 col-form-label">Brand ID</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Brand_id" readonly="readonly">
          </div>
        </div>
        <div class="form-group row">
          <label for="Brand_name" class="col-sm-4 col-form-label">Brand Name</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Brand_name">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-close" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
        <button type="button" class="btn btn-primary btn-sm btn-save"><i class="fas fa-save"></i> Save</button>
        <button type="button" class="btn btn-primary btn-sm btn-update"><i class="fas fa-save"></i> Update</button>
      </div>
    </div>
  </div>
</div>