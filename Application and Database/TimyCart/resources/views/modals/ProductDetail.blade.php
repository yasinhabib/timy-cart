<div class="modal" id="ModalProductDetail" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Insert Product Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="Product_detail_id" class="col-sm-4 col-form-label">Product Detail ID</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Product_detail_id" readonly="readonly">
          </div>
        </div>
        <div class="form-group row">
          <label for="Detail_Product_id" class="col-sm-4 col-form-label">Product ID</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Detail_Product_id" readonly="readonly">
          </div>
        </div>
        <div class="form-group row">
          <label for="Sku_number" class="col-sm-4 col-form-label">SKU Number</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Sku_number">
          </div>
        </div>
        <div class="form-group row">
          <label for="Description" class="col-sm-4 col-form-label">Description</label>
          <div class="col-sm-8">
            <textarea class="form-control" id="Description">
              
            </textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="Specification" class="col-sm-4 col-form-label">Specification</label>
          <div class="col-sm-8">
            <textarea class="form-control" id="Specification">
              
            </textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="Link_rewrite" class="col-sm-4 col-form-label">Link Rewrite</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="Link_rewrite">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm btn-close" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
        <button type="button" class="btn btn-primary btn-sm btn-save"><i class="fas fa-save"></i> Save</button>
        <button type="button" class="btn btn-primary btn-sm btn-update"><i class="fas fa-save"></i> Update</button>
      </div>
    </div>
  </div>
</div>