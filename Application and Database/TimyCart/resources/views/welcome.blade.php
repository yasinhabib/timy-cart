<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="https://pro.fontawesome.com/releases/v5.9.0/css/all.css" rel="stylesheet">

    <!-- JQuery Confirm -->
    <link rel="stylesheet" href="{{ asset('css/jquery-confirm.css') }}">

    <!-- Datatables -->
    <link rel="stylesheet" href="{{ asset('css/datatables.css') }}">
    
</head>
<body>
    <div class="container-fluid">
        <div class="row sticky-top">
            <div class="col-md-12">
                <div class="row desktop-element">
                    <div class="col-md-12 topbar">
                        <div class="container">
                            <div class="row spacer">
                                <div class="col-md-6 text-white clearfix">
                                    <div class="float-left mr-4">
                                        <i class="fal fa-dolly-flatbed mr-2"></i>
                                        <span>FREE SHIPPING</span>
                                    </div>
                                    <div class="float-left mr-4">
                                        <i class="fal fa-watch mr-2"></i>
                                        <span>LIFETIME BATTERY</span>
                                    </div>
                                    <div class="float-left mr-4">
                                        <i class="fal fa-credit-card mr-2"></i>
                                        <span>0% INSTALLMENT</span>
                                    </div>
                                </div>
                                <div class="col-md-6 text-white clearfix">
                                    <div class="float-right">
                                        <span>ONLINE EXCLUSIVE - ALL STRAPS 30% OFF <span class="font-weight-bold">SHOP NOW</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bg-white">
                    <div class="col-md-12 ">
                        <div class="container">
                            <div class="row spacer">
                                <div class="col-md-12">
                                    <nav class="navbar navbar-expand-md navbar-light bg-white p-0">
                                        <a class="navbar-brand" href="/">
                                            <img src="https://thewatch.imgix.net/logos/logo-thewatchco-desktop.png" class="desktop-element desktop-logo"> 
                                            <img src="https://thewatch.imgix.net/logos/logo-thewatchco-mobile.png" class="mobile-element mobile-logo">
                                        </a>

                                        <div class="input-group search-field mobile-element-flex">
                                          <input type="text" class="form-control btn-capsule">
                                          <div class="input-group-append">
                                            <button type="button" class="btn btn-sm input-group-text bg-white btn-capsule"><i class="far fa-search"></i></button>
                                          </div>
                                        </div>
                                        
                                        {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                                              <span class="navbar-toggler-icon"></span>
                                        </button> --}}
                                        <div class="collapse navbar-collapse bg-transparent" id="navbarCollapse">
                                            <ul class="navbar-nav ml-auto justify-content-end">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">WATCHES</a>
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">STRAPS</a>
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">ACCESSORIES</a>
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">JEWERLY</a>
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">BRANDS</a>
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">JOURNAL</a>
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link text-danger" href="#">DISCOUNT</a>
                                                
                                            </ul>
                                            <ul class="navbar-nav ml-auto">
                                            <!-- Authentication Links -->
                                                <li class="nav-item dropdown">
                                                    <a id="navbarDropdownUser" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                        <i class="far fa-user"></i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownUser">
                                                        <a class="dropdown-item" data-toggle="modal" href="#" onclick="event.preventDefault();" data-target="#modalChangePassword">
                                                            Change Password
                                                        </a>
                                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                            {{ __('Logout') }}
                                                        </a>
                                                    </div>
                                                
                                                <li class="nav-item dropdown">
                                                    <a id="navbarDropdownCart" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                        <i class="far fa-shopping-bag"></i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right dropdown-cart" aria-labelledby="navbarDropdownCart">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <span class="font-weight-bold">Total Order : </span>
                                                                    <span>3 item</span>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown-divider"></div>
                                                            <div class="cart-dropdown">
                                                                <div class="row">
                                                                    <div class="col-md-12 d-flex">
                                                                        <div class="cart-image">
                                                                            <img src="https://thewatch.imgix.net/product/10732/10732_medium.jpg" class="cart-product-image rounded">
                                                                        </div>
                                                                        <div class="cart-detail">
                                                                            <span class="d-block font-80">
                                                                                Adidas
                                                                            </span>
                                                                            <span class="d-block font-80">
                                                                                Process_w2 All Black / Gunmetal 
                                                                            </span>
                                                                            <span class="d-block font-80">
                                                                                40mm
                                                                            </span>
                                                                            <div class="row mt-2">
                                                                                <div class="col-md-12 align-middle d-flex">
                                                                                    <span class="font-80 font-weight-bold pt-2" style="width: 50px">Qty : </span>
                                                                                    <button type="button" class="btn btn-white float-left"><i class="far fa-minus"></i></button>
                                                                                    <input type="text" class="form-control form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 text-center" value="1">
                                                                                    <button type="button" class="btn btn-white float-right"><i class="far fa-plus"></i></button>
                                                                                </div>
                                                                            </div>
                                                                            <h6>IDR 2.500.000</h6>
                                                                        </div>
                                                                        <button type="button" class="btn btn-white btn-md float-right"><i class="far fa-times fa-lg"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="dropdown-divider"></div>
                                                                <div class="row">
                                                                    <div class="col-md-12 d-flex">
                                                                        <div class="cart-image">
                                                                            <img src="https://thewatch.imgix.net/product/4154/4154_medium.jpg" class="cart-product-image rounded">
                                                                        </div>
                                                                        <div class="cart-detail">
                                                                            <span class="d-block font-80">
                                                                                Matoa
                                                                            </span>
                                                                            <span class="d-block font-80">
                                                                                Flores - Canadian Maple
                                                                            </span>
                                                                            <div class="row mt-2">
                                                                                <div class="col-md-12 align-middle d-flex">
                                                                                    <span class="font-80 font-weight-bold pt-2" style="width: 50px">Qty : </span>
                                                                                    <button type="button" class="btn btn-white float-left"><i class="far fa-minus"></i></button>
                                                                                    <input type="text" class="form-control form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 text-center" value="1">
                                                                                    <button type="button" class="btn btn-white float-right"><i class="far fa-plus"></i></button>
                                                                                </div>
                                                                            </div>
                                                                            <h6>IDR 1.050.000</h6>
                                                                        </div>
                                                                        <button type="button" class="btn btn-white btn-md float-right"><i class="far fa-times fa-lg"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="dropdown-divider"></div>
                                                                <div class="row">
                                                                    <div class="col-md-12 d-flex">
                                                                        <div class="cart-image">
                                                                            <img src="https://thewatch.imgix.net/product/5465/5465_medium.jpg" class="cart-product-image rounded">
                                                                        </div>
                                                                        <div class="cart-detail">
                                                                            <span class="d-block font-80">
                                                                                Daniel Wellington
                                                                            </span>
                                                                            <span class="d-block font-80">
                                                                                FClassic Petite Sterling 28mm
                                                                            </span>
                                                                            <div class="row mt-2">
                                                                                <div class="col-md-12 align-middle d-flex">
                                                                                    <span class="font-80 font-weight-bold pt-2" style="width: 50px">Qty : </span>
                                                                                    <button type="button" class="btn btn-white float-left"><i class="far fa-minus"></i></button>
                                                                                    <input type="text" class="form-control form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 text-center" value="1">
                                                                                    <button type="button" class="btn btn-white float-right"><i class="far fa-plus"></i></button>
                                                                                </div>
                                                                            </div>
                                                                            <h6>IDR 2.450.000</h6>
                                                                        </div>
                                                                        <button type="button" class="btn btn-white btn-md float-right"><i class="far fa-times fa-lg"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown-divider"></div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span class="d-inline-block float-left align-middle">Total Payment : </span>
                                                                    <h6 class="d-inline-block m-0 align-middle">IDR 6.000.000</h6>
                                                                    <button class="btn btn-sm btn-outline-success d-inline-block float-right btn-capsule align-middle">Checkout</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <li class="nav-item dropdown">
                                                    <a id="navbarDropdownSearch" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                        <i class="far fa-search"></i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSearch" style="width: 250px">
                                                        <form class="p-1 px-2">
                                                            <div class="input-group">
                                                              <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0">
                                                              <div class="input-group-append">
                                                                <button type="button" class="btn btn-sm input-group-text border-0 bg-white"><i class="far fa-arrow-right"></i></button>
                                                              </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row bg-white mobile-element">
                    <div class="col-md-12">
                        <div class="container h-100">
                            <div class="row h-100 justify-content-center align-items-center">
                                <div class="col-md-12 my-2 text-center btn-group-campaign">
                                    <button class="btn btn-outline-dark float-left border-gray">
                                        <i class="fal fa-dolly-flatbed mr-2 float-left align-middle"></i>
                                        <span class="float-left text-left font-60">FREE <br> SHIPPING</span>
                                    </button>
                                    <button class="btn btn-outline-dark border-gray">
                                        <i class="fal fa-watch mr-2 float-left align-middle "></i>
                                        <span class="float-left text-left font-60">LIFETIME <br> BATTERY</span>
                                    </button>
                                    <button class="btn btn-outline-dark float-right border-gray">
                                        <i class="fal fa-credit-card mr-2 float-left align-middle"></i>
                                        <span class="float-left text-left font-60">0% <br> INSTALLMENT</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row desktop-element">
            <div class="col-md-12 p-0">
                <img src="{{asset('images/Desktop/Main-Picture-1.gif')}}" class="mw-100">
            </div>
        </div>
        <div class="row mobile-element">
            <div class="col-md-12 p-0">
                <img src="{{asset('images/Mobile/Q-Timex-Mobile-Final_02.jpg')}}" class="mw-100">
            </div>
        </div>
        <div class="row desktop-element">
            <div class="col-md-12 p-0">
                <img src="{{asset('images/Desktop/Main-Picture-2.gif')}}" class="mw-100 ">
            </div>
        </div>
        <div class="row mobile-element">
            <div class="col-md-12 p-0">
                <img src="{{asset('images/Mobile/Q-Timex-Mobile-Final_03.jpg')}}" class="mw-100 ">
            </div>
        </div>
        <div class="row desktop-element">
            <div class="col-md-12 px-0" style="padding-top: 100px; padding-bottom: 100px">
                <img src="{{asset('images/Desktop/Main-Picture-3.gif')}}" class="mw-100 ">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 p-5">
                            <img src="{{asset('images/Desktop/Tile-Product-Picture.gif')}}" class="mw-100 ">
                        </div>
                        <div class="col-md-6 p-5 text-center">
                            <img src="{{asset('images/Desktop/Product-Picture-Small.gif')}}" class="mw-100">
                            <span class="d-block font-weight-bold" style="padding-top: 35px">Q TIMEX 1979 REISSUE</span>
                            <span class="d-block">Vintage Chronograph Black DLC Case with</span>
                            <span class="d-block">Black Buffalo Leather and Red Stiching</span>
                            <span class="d-block font-weight-bold" style="padding-top: 35px">IDR 2.450.000</span>
                            <span class="d-block" style="color: #94885C">IDR 162.500 / Bulan</span>
                            <button class="btn btn-lg btn-capsule" style="background-color: #206167; width: 250px;margin-top: 35px;"><span class="text-white font-weight-bold">Specs</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 px-0" class="mw-100">
                <img src="{{asset('images/Desktop/Product-Picture-Large.jpg')}}" class="mw-100 ">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 px-0 fix-height">
                <div class="container h-100">
                    <div class="row h-100 justify-content-center align-items-center">
                        <div class="col-md-12 my-4">
                            <p class="text-justify">Times already sold it out their website last May, but we have a very good news for you all : Q Timex will be available for raffle at thewatch.co on 21 June - 5 July 2019 with limited quota and the raffle winner will be announced later at our event at 11 July 2019. It's only 8 pieces available, so you better be fast and ready.</p>
                            <p class="my-4">Terms & Conditions :</p>
                            <table class="border-0">
                                <tr>
                                    <td class="align-top">
                                        1.
                                    </td>
                                    <td>
                                        1 ID Card number be allowed for 1 OTP code (OTP code for re-register during the event).
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-top">
                                        2.
                                    </td>
                                    <td>
                                        The Watch Co. is eligible to close the raffle quota before the end of periods when it reach the maximum quota.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-top">
                                        3.
                                    </td>
                                    <td>
                                        Customer will get email confirmation including the and OTP code by filling the registration form below.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-top">
                                        4.
                                    </td>
                                    <td>
                                        The OTP code cannot be altered & represented.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-top">
                                        5.
                                    </td>
                                    <td>
                                        The raffle winner will be announced at the event, 11 July 2019.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-top">
                                        6.
                                    </td>
                                    <td>
                                        Each raffle winner proceed to do the payment and show their ID card to cashier and receive the product.
                                    </td>
                                </tr>
                            </table>
                            <p class="my-4">Please read carefully our terms and conditions. See you at the event and good luck!</p>
                            <div class="w-100 text-center">
                                <button class="btn btn-lg btn-capsule" style="background-color: #206167; width: 250px;margin-top: 35px;"><span class="text-white font-weight-bold">Register</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer">
            <div class="offset-md-1 col-md-3">
                    <a href="#" class="text-green font-80 d-block">ABOUT</a>
                    <a href="#" class="text-green font-80 d-block">SHIPPING INFORMATION</a>
                    <a href="#" class="text-green font-80 d-block">TERMS & CONDITIONS</a>
                    <a href="#" class="text-green font-80 d-block">FAQ</a>
                    <a href="#" class="text-green font-80 d-block">WARRANTY & SERVICE</a>
                    <a href="#" class="text-green font-80 d-block">CONTACT</a>
                    <a href="#" class="text-green font-80 d-block">STORE LOCATION</a>

                    <div class="social-media">
                        <a href="#" class="text-dark d-inline-block mr-2"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="text-dark d-inline-block mr-2"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="text-dark d-inline-block mr-2"><i class="fab fa-instagram"></i></a>
                        <a href="#" class="text-dark d-inline-block mr-2"><i class="fab fa-pinterest-p"></i></a>
                        <a href="#" class="text-dark d-inline-block mr-2"><i class="fab fa-line"></i></a>
                    </div>
            </div>
            <div class="col-md-4">
                <span class="mb-4 d-block font-80 font-weight-bold">JOIN OUR NEWSLETTER</span>
                <input type="text" class="form-control form-control-sm rounded-0 border-right-0 border-left-0 border-top-0 form-input-transparent w-75 mr-4 d-inline-block" placeholder="Enter your email address">
                <button class="btn btn-outline-dark d-inline-block btn-capsule btn-sm">SUBSCRIBE</button>
                <span class="d-block font-80">and exclusive promotions.</span>
                <span class="d-block font-80">Sign up for our newsletter and stay up to date with our latest releast & event</span>
                <div class="social-media">
                    <div class="row">
                        <div class="col-md-8">
                            <span class="mb-2 d-block font-80 font-weight-bold mb-2">JOIN OUR NEWSLETTER</span>
                            <span class="d-block font-80">Get in touch / +62 813 1234 5678</span>
                            <span class="d-block font-80">(Mon-Sat 10am-8pm CET)</span>
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('images/qr-sample.png')}}" style="width: 100px" class="social-media">
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-4">
                <span class="mb-4 d-block font-80">CICILAN 0%</span>
            </div>
        </div>
        <div class="row fixed-bottom mobile-element">
            <div class="col-md-12 bottom-menu-group">
                <ul class="navbar-nav bottom-menu-group">
                    <li class="nav-item bottom-menu text-center">
                        <a class="nav-link text-white" href="#">
                            <i class="far fa-home-lg-alt d-block"></i>
                            <span class="d-block font-80">Home</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-menu text-center">
                        <a class="nav-link text-white" href="#">
                            <i class="far fa-list-alt d-block"></i>
                            <span class="d-block font-80">Category</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-menu text-center">
                        <a class="nav-link text-white" href="#" id="navbarDropdownCartBottom">
                            <i class="far fa-shopping-cart d-block"></i>
                            <span class="d-block font-80">Cart</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-menu text-center">
                        <a class="nav-link text-white" href="#">
                            <i class="far fa-user d-block"></i>
                            <span class="d-block font-80">User</span>
                        </a>
                    </li>
                    <li class="nav-item bottom-menu text-center">
                        <a class="nav-link text-white" href="#">
                            <i class="far fa-comment-dots d-block"></i>
                            <span class="d-block font-80">Chat</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('js/datatables.js') }}"></script>

    <!-- JQuery Confirm -->
    <script src="{{ asset('js/jquery-confirm.js') }}"></script> 

    <script>
        $('#navbarDropdownCartBottom').on('click',function(){
            $('#cartModal').modal('toggle');
        })
    </script>

    <div class="modal" tabindex="-1" role="dialog" id="cartModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cart</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-weight-bold">Total Order : </span>
                                <span>3 item</span>
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <div class="cart-dropdown">
                            <div class="row">
                                <div class="col-md-12 d-flex">
                                    <div class="cart-image">
                                        <img src="https://thewatch.imgix.net/product/10732/10732_medium.jpg" class="cart-product-image rounded">
                                    </div>
                                    <div class="cart-detail">
                                        <span class="d-block font-80">
                                            Adidas
                                        </span>
                                        <span class="d-block font-80">
                                            Process_w2 All Black / Gunmetal 
                                        </span>
                                        <span class="d-block font-80">
                                            40mm
                                        </span>
                                        <div class="row mt-2">
                                            <div class="col-md-12 align-middle d-flex">
                                                <span class="font-80 font-weight-bold pt-2" style="width: 50px">Qty : </span>
                                                <button type="button" class="btn btn-white float-left"><i class="far fa-minus"></i></button>
                                                <input type="text" class="form-control form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 text-center" value="1">
                                                <button type="button" class="btn btn-white float-right"><i class="far fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <h6>IDR 2.500.000</h6>
                                    </div>
                                    <button type="button" class="btn btn-white btn-md float-right"><i class="far fa-times fa-lg"></i></button>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="row">
                                <div class="col-md-12 d-flex">
                                    <div class="cart-image">
                                        <img src="https://thewatch.imgix.net/product/4154/4154_medium.jpg" class="cart-product-image rounded">
                                    </div>
                                    <div class="cart-detail">
                                        <span class="d-block font-80">
                                            Matoa
                                        </span>
                                        <span class="d-block font-80">
                                            Flores - Canadian Maple
                                        </span>
                                        <div class="row mt-2">
                                            <div class="col-md-12 align-middle d-flex">
                                                <span class="font-80 font-weight-bold pt-2" style="width: 50px">Qty : </span>
                                                <button type="button" class="btn btn-white float-left"><i class="far fa-minus"></i></button>
                                                <input type="text" class="form-control form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 text-center" value="1">
                                                <button type="button" class="btn btn-white float-right"><i class="far fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <h6>IDR 1.050.000</h6>
                                    </div>
                                    <button type="button" class="btn btn-white btn-md float-right"><i class="far fa-times fa-lg"></i></button>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <div class="row">
                                <div class="col-md-12 d-flex">
                                    <div class="cart-image">
                                        <img src="https://thewatch.imgix.net/product/5465/5465_medium.jpg" class="cart-product-image rounded">
                                    </div>
                                    <div class="cart-detail">
                                        <span class="d-block font-80">
                                            Daniel Wellington
                                        </span>
                                        <span class="d-block font-80">
                                            FClassic Petite Sterling 28mm
                                        </span>
                                        <div class="row mt-2">
                                            <div class="col-md-12 align-middle d-flex">
                                                <span class="font-80 font-weight-bold pt-2" style="width: 50px">Qty : </span>
                                                <button type="button" class="btn btn-white float-left"><i class="far fa-minus"></i></button>
                                                <input type="text" class="form-control form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 text-center" value="1">
                                                <button type="button" class="btn btn-white float-right"><i class="far fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <h6>IDR 2.450.000</h6>
                                    </div>
                                    <button type="button" class="btn btn-white btn-md float-right"><i class="far fa-times fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="d-inline-block float-left align-middle">Total Payment : </span>
                                <h6 class="d-inline-block m-0 align-middle">IDR 6.000.000</h6>
                                <button class="btn btn-sm btn-outline-success d-inline-block float-right btn-capsule align-middle">Checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @yield('js')
</body>
</html>