<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->integer('Product_detail_id', true, false)->length(11);
            $table->integer('Product_id')->length(11);
            $table->string('Sku_number');
            $table->text('Description');
            $table->text('Specification');
            $table->string('Link_rewrite');
            $table->foreign('Product_id')->references('Product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
