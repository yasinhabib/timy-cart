<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
	protected $primaryKey = 'Product_image_id';
	
	public $incrementing = false;

    public $timestamps = false;

	
    public function Product()
    {
        return $this->belongsTo('App\Product', 'Product_id', 'Product_id');
    }
}
