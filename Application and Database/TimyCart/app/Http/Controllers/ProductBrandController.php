<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductBrand;
use DB;
use DataTables;

class ProductBrandController extends Controller
{
	public function index(){
		return view('pages.brand');
	}

    public function get(Request $request){
    	$ProductBrand = ProductBrand::all();

    	if ($request->is('api*')) {
	    	return json_encode($ProductBrand);
	    }else{
	    	return Datatables::of($ProductBrand)->make(true);
	    }
    }

    public function getDetail($Brand_id,Request $request){
    	$ProductBrand = ProductBrand::where('Brand_id',$Brand_id)->first();

    	if ($request->is('api*')) {
    		return json_encode($ProductBrand);
    	}else{
	    	return Datatables::of($ProductBrand)->make(true);
	    }
    }

    public function post(Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductBrand = new ProductBrand;
	    	$ProductBrand->Brand_name = $request->Brand_name;
	    	$ProductBrand->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product brand successfully created!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to create product brand ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function put($Brand_id,Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductBrand = ProductBrand::where('Brand_id',$Brand_id)->first();

	    	$ProductBrand->Brand_name = $request->Brand_name;
	    	$ProductBrand->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product brand successfully updated!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to update product brand ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function delete($Brand_id){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductBrand = ProductBrand::where('Brand_id',$Brand_id)->first();
	    	$ProductBrand->delete();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product brand successfully deleted!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to delete product brand ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }
}
