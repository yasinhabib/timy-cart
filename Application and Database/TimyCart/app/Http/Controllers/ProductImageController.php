<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductImage;
use DB;
use DataTables;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ProductImageController extends Controller
{
    public function get(Request $request){
    	$ProductImage = ProductImage::all();

    	if ($request->is('api*')) {
	    	return json_encode($ProductImage);
    	}else{
	    	return Datatables::of($ProductImage)->make(true);
	    }
    }

    public function getDetail($Product_image_id,Request $request){
    	$ProductImage = ProductImage::where('Product_image_id',$Product_image_id)->with('Product')->first();

    	if ($request->is('api*')) {
	    	return json_encode($ProductImage);
    	}else{
	    	return Datatables::of($ProductImage)->make(true);
	    }
    }

    public function post(Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

    		$uploadedFile = $request->file('Product_image_name');

	    	$ProductImage = new ProductImage;
	    	$ProductImage->Product_id = $request->Product_id;
	    	$ProductImage->Product_image_name = $uploadedFile->store('public');
	    	$ProductImage->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product image successfully created!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to create product image ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function put($Product_image_id,Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

    		$uploadedFile = $request->file('Product_image_name');

	    	$ProductImage = ProductImage::where('Product_image_id',$Product_image_id)->first();
	    	$ProductImage->Product_id = $request->Product_id;

	    	if(isset($uploadedFile)){
	    		$currentImage = $ProductImage->Product_image_name;
	      		Storage::delete($currentImage);

		    	$ProductImage->Product_image_name = $uploadedFile->store('public');
		    }

	    	$ProductImage->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product image successfully updated!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to update product image ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function delete($Product_image_id){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductImage = ProductImage::where('Product_image_id',$Product_image_id)->first();

	    	$currentImage = $ProductImage->Product_image_name;
	    	Storage::delete($currentImage);

	    	$ProductImage->delete();

	    	DB::commit();

	    	$message = array(
	    		'status'=>0,
	    		'Message'=>'Product image successfully deleted!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to delete product image ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }
}
