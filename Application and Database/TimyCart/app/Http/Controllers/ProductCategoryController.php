<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use DB;
use DataTables;

class ProductCategoryController extends Controller
{
	public function index(){
		return view('pages.category');
	}

    public function get(Request $request){
    	$ProductCategory = ProductCategory::all();

    	if ($request->is('api*')) {
	    	return json_encode($ProductCategory);
    	}else{
	    	return Datatables::of($ProductCategory)->make(true);
	    }
    }

    public function getDetail($Category_id,Request $request){
    	$ProductCategory = ProductCategory::where('Category_id',$Category_id)->with('Product')->first();

    	if ($request->is('api*')) {
	    	return json_encode($ProductCategory);
    	}else{
	    	return Datatables::of($ProductCategory)->make(true);
	    }
    }

    public function post(Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductCategory = new ProductCategory;
	    	$ProductCategory->Category_name = $request->Category_name;
	    	$ProductCategory->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product category successfully created!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to create product category ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function put($Category_id,Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductCategory = ProductCategory::where('Category_id',$Category_id)->first();
	    	$ProductCategory->Category_name = $request->Category_name;
	    	$ProductCategory->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product category successfully updated!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to update product category ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function delete($Category_id){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductCategory = ProductCategory::where('Category_id',$Category_id)->first();
	    	$ProductCategory->delete();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product category successfully deleted!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to delete product category ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }
}
