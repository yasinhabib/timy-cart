<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;
use DataTables;

class ProductController extends Controller
{
	public function index(){
		return view('pages.product');
	}

    public function get(Request $request){
    	$Product = Product::with('ProductBrand')->with('ProductCategory')->with('ProductDetail')->with('ProductImage')->get();

    	if ($request->is('api*')) {
	    	return json_encode($Product);
	    }else{
	    	return Datatables::of($Product)->make(true);
	    }
    }

    public function getDetail($Product_id,Request $request){
    	$Product = Product::where('Product_id',$Product_id)->with('ProductDetail')->with('ProductImage')->first();

    	if ($request->is('api*')) {
	    	return json_encode($Product);
	    }else{
	    	return Datatables::of($Product)->make(true);
	    }
    }

    public function post(Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$Product = new Product;
	    	$Product->Category_id = $request->Category_id;
	    	$Product->Brand_id = $request->Brand_id;
	    	$Product->Product_Name = $request->Product_Name;
	    	$Product->Product_status = $request->Product_status;
	    	$Product->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product successfully created!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to create product ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function put($Product_id,Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$Product = Product::where('Product_id',$Product_id)->first();
	    	$Product->Category_id = $request->Category_id;
	    	$Product->Product_Name = $request->Product_Name;
	    	$Product->Product_status = $request->Product_status;
	    	$Product->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product successfully updated!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to update product ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function delete($Product_id){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$Product = Product::where('Product_id',$Product_id)->first();
	    	$Product->delete();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Product successfully deleted!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to delete product ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }
}
