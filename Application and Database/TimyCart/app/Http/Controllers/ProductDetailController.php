<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductDetail;
use DB;
use DataTables;

class ProductDetailController extends Controller
{
    public function get(Request $request){
    	$ProductDetail = ProductDetail::all();

    	if ($request->is('api*')) {
	    	return json_encode($ProductDetail);
    	}else{
	    	return Datatables::of($ProductDetail)->make(true);
	    }
    }

    public function getDetail($Product_detail_id,Request $request){
    	$ProductDetail = ProductDetail::where('Product_detail_id',$Product_detail_id)->with('Product')->first();

    	if ($request->is('api*')) {
	    	return json_encode($ProductDetail);
    	}else{
	    	return Datatables::of($ProductDetail)->make(true);
	    }
    }

    public function post(Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductDetail = new ProductDetail;
	    	$ProductDetail->Product_id = $request->Product_id;
	    	$ProductDetail->Sku_number = $request->Sku_number;
	    	$ProductDetail->Description = $request->Description;
	    	$ProductDetail->Specification = $request->Specification;
	    	$ProductDetail->Link_rewrite = $request->Link_rewrite;
	    	$ProductDetail->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Detail product successfully created!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to create detail product ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function put($Product_detail_id,Request $request){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductDetail = ProductDetail::where('Product_detail_id',$Product_detail_id)->first();
	    	$ProductDetail->Product_id = $request->Product_id;
	    	$ProductDetail->Sku_number = $request->Sku_number;
	    	$ProductDetail->Description = $request->Description;
	    	$ProductDetail->Specification = $request->Specification;
	    	$ProductDetail->Link_rewrite = $request->Link_rewrite;
	    	$ProductDetail->save();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Detail product successfully updated!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to update detail product ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }

    public function delete($Product_detail_id){
    	$message = array();

    	try{
    		DB::beginTransaction();

	    	$ProductDetail = ProductDetail::where('Product_detail_id',$Product_detail_id)->first();
	    	$ProductDetail->delete();

	    	DB::commit();

	    	$message = array(
	    		'status'=>1,
	    		'Message'=>'Detail product successfully deleted!'
	    	);
	    	return json_encode($message);
    	}catch(\Exception $e){
            DB::rollback();
            
            $message = array(
	    		'status'=>0,
	    		'Message'=>"Failed to delete detail product ! \n Error Message: ".$e->getMessage()
	    	);
	    	return json_encode($message);
        }    
    }
}
