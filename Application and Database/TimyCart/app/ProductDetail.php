<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
	protected $primaryKey = 'Product_detail_id';
	
	public $incrementing = false;

    public $timestamps = false;

	
    public function Product()
    {
        return $this->belongsTo('App\Product', 'Product_id', 'Product_id');
    }
}
