<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'Product_id';
    
    public $incrementing = false;

    public $timestamps = false;


    public function ProductCategory()
    {
        return $this->belongsTo('App\ProductCategory', 'Category_id', 'Category_id');
    }

    public function ProductBrand()
    {
        return $this->belongsTo('App\ProductBrand', 'Brand_id', 'Brand_id');
    }

    public function ProductDetail()
    {
        return $this->hasMany('App\ProductDetail', 'Product_id', 'Product_id');
    }

    public function ProductImage()
    {
        return $this->hasMany('App\ProductImage', 'Product_id', 'Product_id');
    }
}
