<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
	protected $primaryKey = 'Brand_id';

    public $incrementing = false;

    public $timestamps = false;

    public function Product()
    {
        return $this->hasMany('App\Product', 'Brand_id', 'Brand_id');
    }
}
