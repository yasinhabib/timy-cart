<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	protected $primaryKey = 'Category_id';

	public $incrementing = false;

    public $timestamps = false;

    public function Product()
    {
        return $this->hasMany('App\Product', 'Category_id', 'Category_id');
    }
}
