Step untuk menjalankan aplikasi

1. buat database dengan nama dbase_twc_store
2. import file dbase_twc_store.sql ke dalam database dbase_twc_store
4. buka folder TimyCart dan buka file.env
5. ganti konfigurasi database jika setting database berbeda dengan config yang ada di .ENV
3. buka cmd dan akses ke dalam folder TimyCart
4. jalan kan syntax php artisan config:cache
5. jalankan syntax php artisan serve
6. aplikasi sudah berjalan sempurna ketika tertulis Laravel development server started: <http://127.0.0.1:8000>
7. buka browser dan akses halaman localhost:8000 untuk mengakses aplikasi
8. untuk daftar url api dapat dilihat pada file api.php pada folder TimyCart/routes